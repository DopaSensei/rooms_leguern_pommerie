package fr.iut.rm.persistence.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Number of places occupied
     */
    @Min(0)
    private int nbPlacesOccupied;

    /**
     * Number of places total
     */
    @Min(1)
    private int capacity;

    /**
     * Room's name
     */
    @Column(nullable = false, unique = true) @Size(min = 3, max = 30)
    private String name;

    /**
     * Default constructor (do nothing)
     */
    public Room() {
        // Do nothing
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the number of places occupied
     */
    public int getNbPlacesOccupied() {
        return nbPlacesOccupied;
    }

    /**
     * anemic setter
     *
     * @param nbPlacesOccupied the new calling number to set
     */
    public void setNbPlacesOccupied(final int nbPlacesOccupied) {
        this.nbPlacesOccupied = nbPlacesOccupied;
    }

    /**
     * anemic getter
     *
     * @return the number of places occupied
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * anemic setter
     *
     * @param capacity the new calling number to set
     */
    public void setCapacity(final int capacity) {
        this.capacity = capacity;
    }

    /**
     * Allow people to join a room
     */
    public void joinARoom() {nbPlacesOccupied++; }

    /**
     * Allow people to leave a room
     */
    public void leaveARoom() {nbPlacesOccupied--; }
    
    /**
     * anemic getter
     *
     * @return the number of places occupied
     */
    public int getAvailablePlacesRatio() {
        return (100 * nbPlacesOccupied) / capacity;
    }

    /**
     * anemic getter
     *
     * @return the calling number
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new calling number to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Leave a room
     * @return true if the guy who tried to leave the room actually managed to do it.
     */
    public boolean leaveRoom(){
        if(nbPlacesOccupied > 0){
            nbPlacesOccupied--;
            return true;
        }
        return false;
    }

    /**
     * Join a room
     * @return true if the guy who tried to join the room actually managed to do it.
     */
    public boolean joinRoom(){
        if(nbPlacesOccupied < capacity){
            nbPlacesOccupied++;
            return true;
        }
        return false;
    }

}
