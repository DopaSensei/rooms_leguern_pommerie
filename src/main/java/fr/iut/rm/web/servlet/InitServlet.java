package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class InitServlet extends HttpServlet {

    /**
     * Gathers the data handler
     */
    @Inject
    RoomDao roomsHandlerDAO;
    /**
     * HTTP GET access
     *
     * @param req nothing required
     * @param resp response to sent
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException      by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        int roomsNumber = 1;
        String param = req.getParameter("nb");
        if(roomsHandlerDAO.findAll().isEmpty()) {

            if (param != null) {
                roomsNumber = Integer.parseInt(param);
            }
            if (roomsNumber == 1) {
                Room room = new Room();
                room.setName("Amphi 2");
                room.setNbPlacesOccupied(315);
                room.setCapacity(350);
                roomsHandlerDAO.saveOrUpdate(room);

                Room room2 = new Room();
                room2.setName("Salle 207");
                room2.setNbPlacesOccupied(27);
                room2.setCapacity(40);
                roomsHandlerDAO.saveOrUpdate(room2);

            } else {
                Room room1 = new Room();
                room1.setName("301");
                roomsHandlerDAO.saveOrUpdate(room1);

                Room room2 = new Room();
                room2.setName("200");
                roomsHandlerDAO.saveOrUpdate(room2);

                Room room3 = new Room();
                room3.setName("Amphi 4");
                roomsHandlerDAO.saveOrUpdate(room3);

            }
        }

        String toRedirect = getServletContext().getContextPath() + "/rooms";
        resp.sendRedirect(toRedirect);
    }
}
