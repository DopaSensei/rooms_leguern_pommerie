<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<h1>${title}</h1>

<ul>
<#list rooms as room>
    <div class="well">
        <form method="GET" role="form">
            <#if room.getAvailablePlacesRatio() < 40 >


                <li class="list-group-item list-group-item-success">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                <div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                    Filled with ${room.getAvailablePlacesRatio()} %
                </div>
                </div>
                <#if room.getAvailablePlacesRatio() != 100>
                <p><a href="rooms?name=${room.getName()}&action=in">Enter</a>

                </#if>

                <#if room.getAvailablePlacesRatio() != 0>
                <p><a href="rooms?n=${room.getName()}&a=out">Leave</a>
                </#if>

            <#elseif room.getAvailablePlacesRatio() < 70 >


                <li class="list-group-item list-group-item-warning">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                <div class="progress"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                    Filled with ${room.getAvailablePlacesRatio()} %
                </div>
                </div>
                <#if room.getAvailablePlacesRatio() != 100>
                <p><a href="rooms?name=${room.getName()}&action=in">Enter</a>

                </#if>

                <#if room.getAvailablePlacesRatio() != 0>
                <p><a href="rooms?n=${room.getName()}&a=out">Leave</a>
                </#if>
            <#else>
                <li class="list-group-item list-group-item-danger">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                <div class="progress"><div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                    Filled with ${room.getAvailablePlacesRatio()} %
                </div>
                </div>
                <#if room.getAvailablePlacesRatio() != 100>
                <p><a href="rooms?name=${room.getName()}&action=in">Enter</a>

                </#if>

                <#if room.getAvailablePlacesRatio() != 0>
                <p><a href="rooms?n=${room.getName()}&a=out">Leave</a>
                </#if>
            </#if>
        </form>
    </div>
</#list>
</ul>

<hr>

<input type="button" name="index" value="Go back to index" onclick="self.location.href='index'" >
<input type="button" name="Initialize rooms" value="Initialize rooms" onclick="self.location.href='init'" >
<input type="button" name="administrate" value="Administration" onclick="self.location.href='admin'" >

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>