<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <h1>Administrate rooms</h1>

    <ul>
    <#list rooms as room>
        <div class="well">
            <form method="GET" role="form">
                <#if room.getAvailablePlacesRatio() < 40 >
                    <li class="list-group-item list-group-item-success">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                    <div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                        Filled with ${room.getAvailablePlacesRatio()} %
                         </div>
                    </div>

                <#elseif room.getAvailablePlacesRatio() < 70 >
                    <li class="list-group-item list-group-item-warning">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                        <div class="progress"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                            Filled with ${room.getAvailablePlacesRatio()} %
                             </div>
                        </div>
                <#else>
                    <li class="list-group-item list-group-item-danger">The room ${room.getName()} currently has ${room.getNbPlacesOccupied()} people over ${room.getCapacity()} seats.</li>
                        <div class="progress"><div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="${room.getAvailablePlacesRatio()}" aria-valuemin="0" aria-valuemax="100" style="width:${room.getAvailablePlacesRatio()}%">
                            Filled with ${room.getAvailablePlacesRatio()} %
                             </div>
                        </div>
                </#if>
            </form>



        </div>
    </#list>
    </ul>

    <h1>${create}</h1>
        <form method="GET" role="form">
           <p>
            <label for="pseudo">Room name</label>
            <input type="text" name="roomName" id="roomName" />
            <br />
            <label for="pseudo">Places in room :</label>
            <input type="text" name="placesNumber" id="placesNumber" />
            <input type="submit" value="Add this room">
            <input type="reset" value="Reset this form">
           </p>
        </form>

    <h1>${suppr}</h1>
        <form method="GET" role="form">

            <label>Choose a room :</label>
            <select list="listofRooms" name="remove">
             <datalist id="listofRooms">
                 <#list rooms as room>
                        <option>${room.getName()}</option>
                 </#list>
             </datalist>
             </select>
            <input type="submit" value="Delete this room">
        </form>


    <hr>

    <input type="button" name="index" value="Home" onclick="self.location.href='index'" >

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>